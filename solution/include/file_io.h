#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR,
};

enum open_status open_read_file(const char *filename, FILE **stream);

enum open_status open_write_file(const char *filename, FILE **stream);

#endif//ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
