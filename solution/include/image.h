#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};


void image_rotate(struct image const *image, struct image *rotated_image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
