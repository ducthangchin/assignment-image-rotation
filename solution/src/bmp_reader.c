#include "../include/image.h"
#include "../include/bmp_reader.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header
{
uint16_t bfType;
uint32_t bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t biHeight;
uint16_t biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t padding_calculator( struct image const *image) {
    return image -> width % 4;
}


struct bmp_header* init_header( struct image const* img ) {

    struct bmp_header* header = malloc(sizeof(struct bmp_header));

    header -> bfType = 0x4D42;
    header -> bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel)*img->width + padding_calculator(img))*img->height;
    header -> bfReserved = 0;
    header -> bOffBits = sizeof(struct bmp_header);
    header -> biSize = 40;
    header -> biWidth = img->width;
    header -> biHeight = img->height;
    header -> biPlanes = 1;
    header -> biBitCount = 24;
    header -> biCompression = 0;
    header -> biSizeImage = sizeof(struct pixel)*img->width*img->height;
    header -> biXPelsPerMeter = 0;
    header -> biYPelsPerMeter = 0;
    header -> biClrUsed = 0;
    header -> biClrImportant = 0;

    return header;
}

enum read_status from_bmp( FILE* in, struct image* img ) {

    struct bmp_header* header = malloc(sizeof(struct bmp_header));

    fread(header, sizeof(struct bmp_header), 1, in);

    if (header -> biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    if (header->bfType != 0x4D42)
    {
        return READ_INVALID_SIGNATURE;
    }

    if (header -> biSize != 40 || header -> bOffBits + header -> biSizeImage !=
    header -> bfileSize) {
        return READ_INVALID_HEADER;
    }


    img -> width = header -> biWidth;
    img -> height = header -> biHeight;
    img -> data = malloc(sizeof(struct pixel) * img -> width * img -> height);

    uint8_t padding = padding_calculator(img);
    uint32_t rowLength = img -> width;
    uint32_t padded_row_size = sizeof(struct pixel) * rowLength + padding;

    for(uint32_t i=0; i<header->biHeight; i++) {

        fseek(in, (int32_t)(sizeof(struct bmp_header)+padded_row_size*i),SEEK_SET);

        fread(img->data+i*rowLength, sizeof(struct pixel), img->width, in);

    }

    free(header);
    fclose(in);

    return READ_OK;
}




enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header* header = init_header(img);

    fwrite(header, sizeof(struct bmp_header), 1, out);

    uint8_t padding = padding_calculator(img);
    uint32_t filler = 0;

    for(uint32_t i = 0; i < img -> height; i++) {
        fwrite(img -> data + i * img -> width, sizeof(struct pixel) * img -> width, 1, out);
        fwrite(&filler, padding, 1, out);
    }

    free(header);
    fclose(out);

    return WRITE_OK;
}
