#include "../include/image.h"
#include <stdlib.h>


void image_free(struct image *im) {
    free(im->data);
}

void image_rotate(struct image const *image, struct image *rotated_image) {

    rotated_image->height = image->width;
    rotated_image->width = image->height;
    rotated_image->data = malloc(sizeof(struct pixel) * image->width * image->height);

    uint32_t m = image -> height;
    uint32_t n = image -> width;

    for(size_t i = 0; i < m; i++) {
        for (size_t j = 0; j < n; j++){
            rotated_image->data[i + j*m] = image -> data[(m-i-1)*n +j];
        }
    }

}
