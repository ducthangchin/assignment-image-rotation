#include "bmp_reader.h"
#include "file_io.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    (void) argc;// suppress 'unused parameters' warning
    FILE *file_in, *file_out;
    struct image image, rotated;

    open_read_file(argv[1], &file_in);

    from_bmp(file_in, &image);

    image_rotate(&image, &rotated);

    open_write_file(argv[2], &file_out);

    to_bmp(file_out, &rotated);

    free(image.data);
    free(rotated.data);

    return 0;
}

