#include "../include/file_io.h"
#include <stdio.h>
#include <unistd.h>


enum open_status open_read_file(const char *filename, FILE **stream) {
    if(access(filename, F_OK) != -1){
        *stream = fopen(filename, "rb");
        return OPEN_OK;
    }
    return OPEN_ERROR;
}

enum open_status open_write_file(const char *filename, FILE **stream) {
    *stream = fopen(filename, "wb");
    return OPEN_OK;
}
